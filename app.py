from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/red55')
    def my():
        return '55'

    @app.route('/status')
    def healthcheck():
        return 'OK'

    @app.route('/red00')
    def my_route():
        return '00'

    @app.route('/red17')
    def red17():
        return '17'

    @app.route('/red10')
    def red10():
        return '10'

    @app.route('/red05')
    def foo05():
        return '05 connected'

    @app.route('/red16')
    def red16():
        return '16'

    return app


app = create_app()
